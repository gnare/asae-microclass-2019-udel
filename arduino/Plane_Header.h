#ifdef PLANE_HEADER_H
#define PLANE_HEADER_H

#define GYRO_I2C ADDR       0x01
#define ACCEL_I2C_ADDR      0x02
#define COMPASS_I2C_ADDR    0x03

#define THROTTLE_PIN        4
#define AILERON_L_PIN       5
#define AILERON_R_PIN       6
#deinfe RUDDER_PIN          7

#define CONTROLLER_RADIO_ADDR 0xff
#define PLANE_RADIO_ADDR 0xfe

#define PacketStructures
struct Controller_Packet {
    bool cargoBay;
    bool stabilityAssist;
    uint8_t throttle;
    int8_t yaw;
    int8_t roll;
    int8_t pitch;
}

struct Plane_Packet {
    bool cargoBay;
    bool stabilityAssist;
    uint8_t throttle;
    int8_t yaw;
    int8_t roll;
    int8_t pitch;
    uint8_t voltage;
    uint8_t altitude;
}

#endif
